Describe el comportamiento en la ejecución al hacer el cambio del inciso a.
-La Producción se acelera mientras que la extracción es mas tardada.

Describe el comportamiento en la ejecución al hacer el cambio del inciso b.
-La Extraccion es muy rapida, sin embargo, la produccion es muy lenta.

¿El buffer se llena o se vacía con el cambio del inciso a.?¿Cómo lo puedes
comprobar en la ejecución?
-El Buffer se llena ya que la produccion es mucho mas rapida que la extraccion.

¿El buffer se llena o se vacía con el cambio del inciso b.?¿Cómo lo puedes
comprobar en la ejecución?
-El Buffer se vacía ya que la extracción es mas rapida que la produccion.