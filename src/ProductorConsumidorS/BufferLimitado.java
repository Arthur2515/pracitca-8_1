package ProductorConsumidorS;

public class BufferLimitado
{
	final int size = 10;
	
	double buffer[] = new double[size];
	
	int inBuf = 0, outBuf = 0;
	
	SemaforoBinario mutex = new SemaforoBinario(true);
	
	SemaforoContador isEmpty = new SemaforoContador(0);
	
	SemaforoContador isFull = new SemaforoContador(size);
	
	public void deposit(double value)
	{
		isFull.P();
		
		mutex.P();
		
		buffer[inBuf] = value;
		
		inBuf = (inBuf + 1) % size;
		
		mutex.V();
		
		isEmpty.V();
	}
	
	public double fetch()
	{
		double value;
		
		isEmpty.P();
		
		mutex.P();
		
		value = buffer[outBuf];
		
		outBuf = (outBuf+1) % size;
		
		mutex.V();
		
		isFull.V();
		
		return value;
	}
}
